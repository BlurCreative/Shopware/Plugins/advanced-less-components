<?php
namespace BlurLessComponents;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\UninstallContext;

class BlurLessComponents extends Plugin
{

    /**
    * {@inheritdoc}
    */
    public function uninstall(UninstallContext $context): void
    {

        $this->secureUninstall();

        if (!$context->keepUserData()) {
            $this->removeAllTables();
            $this->someOtherDestructiveMethod();
        }

        // Clear only cache when switching from active state to uninstall
        if ($context->getPlugin()->getActive()) {
            $context->scheduleClearCache(UninstallContext::CACHE_LIST_ALL);
        }
    }
}
?>
