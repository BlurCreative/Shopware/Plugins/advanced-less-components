# Advanced LESS Components

This Extension extends Themes and Plugin by several LESS Components and Variables.  
These Components and Variables are Global available. In every Plugin and Theme LESS File.

Example
``` css
	<div class="d-block">
		...
	</div>
```
*Default Usage. Style apply to all breakpoints*

Example
``` css
	<div class="d-sm-block">
		...
	</div>
```
*Style apply to small ( @phoneLandscapeViewportWidth ) breakpoint and upwards*

### Display Flex
- `.d-{breakpont}-flex`
- `.d-inline-{breakpont}-flex`

### Align Items
- `.align-items-{breakpont}-start`
- `.align-items-{breakpont}-center`
- `.align-items-{breakpont}-end`
- `.align-items-{breakpont}-stretch`