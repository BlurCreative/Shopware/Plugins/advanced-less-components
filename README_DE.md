# Less Komponenten Biblothek

Dieses Plugin erweitert die Themes um verschiedene Kommponenten und Variablen.  
Diese Komponenten sind Global, also in allen Themes und Plugin LESS Dateien des Shops, verfügbar.  

### Enthaltene Komponenten
- Flexbox
- Input Group